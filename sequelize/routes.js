const { Router } = require("express");
const controller = require("./controller.js");
const router = Router();
router.get("", controller.getTodos);
router.get("/:id", controller.getTodosById);
router.post("/", controller.addTodo);
router.put("/:id", controller.updateTodo);
router.delete("/:id", controller.deleteTodo);
module.exports = router;
