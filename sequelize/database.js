const { Sequelize } = require("sequelize");
//REQUIRE DOTENV
//CONFIGURE DOTENV
const dotenv = require("dotenv");
dotenv.config();
const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.USER_NAME,
  process.env.PASSWORD,
  {
    dialect: "postgres",
    host: "localhost",
    port: 5432,
  }
);

module.exports = sequelize;
