const db = require("./models/index.js");
const yup = require("yup");
const schema = yup.object().shape({
  text: yup.string().required(),
  isCompleted: yup.boolean().required(),
});
const todo1s = db.todo1;
async function getTodos(req, res) {
  try {
    const result = await todo1s.findAll();
    res.status(200).send(result);
  } catch (error) {
    console.log(error);
  }
}
async function getTodosById(req, res) {
  try {
    const result = await todo1s.findByPk(req.params.id);
    if (!result) {
      res.status(404).send({ message: "No todo Found" });
    }
    res.status(200).send(result);
  } catch (error) {
    console.log(error);
  }
}
async function addTodo(req, res) {
  try {
    const { text, isCompleted } = req.body;
    let data = {
      text: text,
      isCompleted: isCompleted,
    };
    schema
      .validate(data)
      .then(async (response) => {
        await todo1s.create(data);
        res.status(201).send("Created new todo");
      })
      .catch((err) => {
        console.log(err);
        res.status(400).send("not a valid todo");
      });
  } catch (error) {
    console.log(error);
  }
}
async function deleteTodo(req, res) {
  try {
    await todo1s
      .findByPk(req.params.id)
      .then(async (response) => {
        if (response) {
          const id = parseInt(req.params.id);
          await todo1s.destroy({
            where: {
              id: id,
            },
          });
          res.status(200).send(`deleted todo with id ${id}`);
        } else {
          res.status(404).send("Not Found");
        }
      })
      .catch((err) => {
        console.log("catch", err);
      });
  } catch (error) {
    console.log(error);
  }
}
async function updateTodo(req, res) {
  try {
    const id = parseInt(req.params.id);
    const { text, isCompleted } = req.body;
    let data = {
      text: text,
      isCompleted: isCompleted,
    };
    await schema.validate({ text, isCompleted });
    const todo = await todo1s.findByPk(id);
    if (!todo) {
      res.status(400).send("No todo with that id");
    }
    await todo1s.update(data, { where: { id: id } });
    res.status(200).send("updated todo");
  } catch (err) {
    console.log(err);
    res.status(400).send("not a valid todo");
  }
}
module.exports = {
  getTodos,
  getTodosById,
  addTodo,
  deleteTodo,
  updateTodo,
};
