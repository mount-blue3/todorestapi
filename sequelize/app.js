const express = require("express");
const database = require("./database.js");
const db = require("./models/index.js");
const routes = require("./routes.js");
const todo1 = db.todo1;
const app = express();
const port = 7000;

database
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch((error) => {
    console.error("Unable to connect to the database:", error);
  });

app.use(express.json());
app.use("/todos", routes);

app.listen(port, () => console.log(`app listening on port ${port}`));
